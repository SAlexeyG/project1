﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureScript : MonoBehaviour
{
    private Vector3 __direction = new Vector3(0f, -1f, 0f);
    private Vector3 __position = new Vector3(-1f, 4f, 0f);

    private static int count = 0;

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(__direction * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (count == 2)
        {
            __direction = new Vector3(0f, 0f, 0f);
            return;
        }

        if (collision.collider.name.Contains("ChildFigure") | collision.collider.name == "BottomBorder")
        {
            __direction = new Vector3(0f, 0f, 0f);
            Instantiate(this.gameObject).transform.position = __position;
            count++;
        }
    }
}
